import React, { Component } from "react";

import { CREDENTIALS } from "./constants/algolia";
import { withSearch } from "./lib";

import Input from "./example/InputBox";
import HitsBox from "./example/HitsBox";
import SortBox from "./example/SortBox";
import FilterBox from "./example/FilterBox";
import SelectBox from "./example/SelectBox";
import FilterListBox from "./example/FilterListBox";

import logo from "./logo.svg";
import "./App.css";

/**
 *
 */
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <FilterBox attribute={"skills"} />
        <SelectBox attribute={"jobType"} />
        <SortBox
          defaultRefinement={"job"}
          items={[
            { value: "job", label: "Relevant" },
            { value: "job_by_date", label: "Recent" }
          ]}
        />
        <FilterListBox />
        <Input />
        <HitsBox />
      </div>
    );
  }
}

export default withSearch(App, {
  apiKey: CREDENTIALS.API_KEY,
  appId: CREDENTIALS.APP_ID,
  indexName: "job"
});
