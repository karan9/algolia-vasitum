import React from "react";
import { withSearchBox } from "../../lib";

const Input = ({ refine, currentRefinement }) => {
  return (
    <input value={currentRefinement} onChange={e => refine(e.target.value)} />
  );
};

export default withSearchBox(Input);
