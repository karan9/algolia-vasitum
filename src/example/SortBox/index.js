import React from "react";
import { withSortBy } from "../../lib";

const SortBox = ({ refine, currentRefinement }) => {
  return (
    <div>
      <select onChange={e => refine(e.target.value)} value={currentRefinement}>
        <option value="job">Relevant</option>
        <option value="job_by_date">Recent</option>
      </select>
    </div>
  );
};

export default withSortBy(SortBox);
