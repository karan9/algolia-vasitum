import React from "react";
import { withFilter } from "../../lib";

const FilterBox = ({ items, filter, onSelect }) => {
  if (!items) {
    console.error("Items, not Found", items);
    return null;
  }

  return (
    <div>
      {items.map(item => {
        return (
          <div
            style={{
              padding: "5px",
              width: "300px",
              cursor: "pointer"
            }}
            key={item.label}
            onClick={e => onSelect(item.value)}
          >
            {item.label} {item.count}
          </div>
        );
      })}
    </div>
  );
};

export default withFilter(FilterBox);
