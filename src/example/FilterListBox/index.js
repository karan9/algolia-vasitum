import React from "react";
import { withCurrentFilters } from "../../lib";

const FilterListBox = ({ refine, items }) => {
  return (
    <React.Fragment>
      <div>
        {items.map(item => {
          if (Array.isArray(item.items)) {
            return item.items.map(val => {
              return (
                <div
                  style={{
                    cursor: "pointer"
                  }}
                  onClick={e => refine(val.value)}
                >
                  {val.label}
                </div>
              );
            });
          }

          return (
            <div
              style={{
                cursor: "pointer"
              }}
              onClick={e => refine(item)}
            >
              {item.label}
            </div>
          );
        })}
      </div>
      <div
        style={{
          cursor: "pointer"
        }}
        onClick={e => refine(items)}
      >
        Total Applied Filters {items.length}
      </div>
    </React.Fragment>
  );
};

export default withCurrentFilters(FilterListBox);
