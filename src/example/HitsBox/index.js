import React from "react";
import { withHits } from "../../lib/";

class HitsBox extends React.Component {
  render() {
    return this.props.hits.map(val => {
      return (
        <div
          style={{
            padding: "5px",
            border: "1px solid black",
            width: "300px",
            margin: "10px auto"
          }}
          key={val.objectID}
        >
          {val.title}
        </div>
      );
    });
  }
}

export default withHits(HitsBox);
