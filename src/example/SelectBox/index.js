import React from "react";
import { withSelect } from "../../lib";

const SelectBox = ({ items, currentRefinement, onSelect }) => {
  console.log(items);

  return (
    <select
      value={currentRefinement ? currentRefinement : ""}
      onChange={e => onSelect(e.target.value)}
    >
      <option value="">All</option>
      {items.map(item => {
        return (
          <option key={item.label} value={item.label}>
            {item.label}
          </option>
        );
      })}
    </select>
  );
};

export default withSelect(SelectBox);
