import React from "react";
import { connectCurrentRefinements } from "react-instantsearch-dom";

function withCurrentFilters(WrappedComponent) {
  return connectCurrentRefinements(
    class extends React.Component {
      render() {
        return <WrappedComponent {...this.props} />;
      }
    }
  );
}

export default withCurrentFilters;
