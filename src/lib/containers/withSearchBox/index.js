import React from "react";
import { connectSearchBox } from "react-instantsearch-dom";

function withSearchBox(WrappedComponent) {
  return connectSearchBox(
    class extends React.Component {
      render() {
        /**
         * @prop refine: function
         */
        return <WrappedComponent {...this.props} />;
      }
    }
  );
}

export default withSearchBox;
