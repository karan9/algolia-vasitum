import React from "react";
import { InstantSearch } from "react-instantsearch-dom";
import qs from "qs";

const urlToSearchState = location => qs.parse(location.search.slice(1));
const createURL = state => `?${qs.stringify(state)}`;

function withSearch(
  WrappedComponent,
  { appId, apiKey, indexName, ...algoliaProps }
) {
  return class extends React.Component {
    state = {
      searchState: urlToSearchState(this.props.location)
    };

    componentWillReceiveProps(props) {
      if (props.location !== this.props.location) {
        this.setState({
          searchState: urlToSearchState(props.location)
        });
      }
    }

    onSearchStateChange = searchState => {
      this.props.history.push({
        pathname: this.props.location.pathname,
        search: qs.stringify(searchState),
        state: searchState
      });
      this.setState({ searchState });
    };

    render() {
      return (
        <InstantSearch
          {...algoliaProps}
          appId={appId}
          apiKey={apiKey}
          indexName={indexName}
          searchState={this.state.searchState}
          onSearchStateChange={this.onSearchStateChange}
          createURL={createURL}
        >
          <WrappedComponent {...this.props} />
        </InstantSearch>
      );
    }
  };
}

export default withSearch;
