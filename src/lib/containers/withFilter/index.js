import React from "react";
import { connectRefinementList } from "react-instantsearch-dom";
import PanelCallbackHandler from "../../utils/PanelCallbackHandler";

function withFilter(WrappedComponent) {
  return connectRefinementList(
    class extends React.Component {
      onSelectItem = value => {
        this.props.refine(value);
      };

      render() {
        return (
          <PanelCallbackHandler {...this.props}>
            <WrappedComponent
              {...this.props}
              filter={this.props.attribute}
              onSelect={this.onSelectItem}
            />
          </PanelCallbackHandler>
        );
      }
    }
  );
}

export default withFilter;
