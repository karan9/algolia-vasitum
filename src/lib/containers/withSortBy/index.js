import React from "react";
import { connectSortBy } from "react-instantsearch-dom";

function withSortBy(WrappedComponent) {
  return connectSortBy(
    class extends React.Component {
      render() {
        return <WrappedComponent {...this.props} />;
      }
    }
  );
}

export default withSortBy;
