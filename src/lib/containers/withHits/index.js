import React from "react";
import { connectHits } from "react-instantsearch-dom";

function withHits(WrappedComponent) {
  return connectHits(
    class extends React.Component {
      render() {
        return <WrappedComponent {...this.props} />;
      }
    }
  );
}

export default withHits;
